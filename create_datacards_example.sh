#!/bin/bash

#Please update paths in this macro. It is intented as a memo, in order to create the right datacarads for the stat analysis.

echo "Cleaning..."
rm -f datacard_*2016.txt shapes_*2016.root workspace*.root higgsCombine*.root

echo "Signal region..."
create_datacard.py /user/npostiau/HZZ2l2nu_Fall19/hzz2l2nu/WeightsAndDatadriven/template_dilepton_2016_Asimov_v5_withInstrMET.root --channels eq0jets eq1jets geq2jets_discrbin1 geq2jets_discrbin2 geq2jets_discrbin3 geq2jets_discrbin4 geq2jets_discrbin5 geq2jets_discrbin6 geq2jets_discrbin7 --datacard datacard_SR_2016.txt --shapes shapes_SR_2016.root --year 2016

for NUMBER in {1..14}
do
  echo ${NUMBER}
  create_datacard.py /user/npostiau/HZZ2l2nu_Fall19/hzz2l2nu/WeightsAndDatadriven/template_photon_2016_v5_bin_${NUMBER}.root --channels eq0jets eq1jets geq2jets_discrbin1 geq2jets_discrbin2 geq2jets_discrbin3 geq2jets_discrbin4 geq2jets_discrbin5 geq2jets_discrbin6 geq2jets_discrbin7 --datacard datacard_CR_bin${NUMBER}_2016.txt --shapes shapes_CR_bin${NUMBER}_2016.root --year 2016
  echo R_eq0jets_bin${NUMBER}_2016 rateParam eq0jets InstrMET_bin${NUMBER} 100 [0,999] >> datacard_SR_2016.txt
  echo R_eq0jets_bin${NUMBER}_2016 rateParam eq0jets InstrMET_bin${NUMBER} 100 [0,999] >> datacard_CR_bin${NUMBER}_2016.txt
  echo R_eq1jets_bin${NUMBER}_2016 rateParam eq1jets InstrMET_bin${NUMBER} 100 [0,999] >> datacard_SR_2016.txt
  echo R_eq1jets_bin${NUMBER}_2016 rateParam eq1jets InstrMET_bin${NUMBER} 100 [0,999] >> datacard_CR_bin${NUMBER}_2016.txt
  echo R_geq2jets_discrbin1_bin${NUMBER}_2016 rateParam geq2jets_discrbin1 InstrMET_bin${NUMBER} 100 [0,999] >> datacard_SR_2016.txt
  echo R_geq2jets_discrbin1_bin${NUMBER}_2016 rateParam geq2jets_discrbin1 InstrMET_bin${NUMBER} 100 [0,999] >> datacard_CR_bin${NUMBER}_2016.txt
  echo R_geq2jets_discrbin2_bin${NUMBER}_2016 rateParam geq2jets_discrbin2 InstrMET_bin${NUMBER} 100 [0,999] >> datacard_SR_2016.txt
  echo R_geq2jets_discrbin2_bin${NUMBER}_2016 rateParam geq2jets_discrbin2 InstrMET_bin${NUMBER} 100 [0,999] >> datacard_CR_bin${NUMBER}_2016.txt
  echo R_geq2jets_discrbin3_bin${NUMBER}_2016 rateParam geq2jets_discrbin3 InstrMET_bin${NUMBER} 100 [0,999] >> datacard_SR_2016.txt
  echo R_geq2jets_discrbin3_bin${NUMBER}_2016 rateParam geq2jets_discrbin3 InstrMET_bin${NUMBER} 100 [0,999] >> datacard_CR_bin${NUMBER}_2016.txt
  echo R_geq2jets_discrbin4_bin${NUMBER}_2016 rateParam geq2jets_discrbin4 InstrMET_bin${NUMBER} 100 [0,999] >> datacard_SR_2016.txt
  echo R_geq2jets_discrbin4_bin${NUMBER}_2016 rateParam geq2jets_discrbin4 InstrMET_bin${NUMBER} 100 [0,999] >> datacard_CR_bin${NUMBER}_2016.txt
  echo R_geq2jets_discrbin5_bin${NUMBER}_2016 rateParam geq2jets_discrbin5 InstrMET_bin${NUMBER} 100 [0,999] >> datacard_SR_2016.txt
  echo R_geq2jets_discrbin5_bin${NUMBER}_2016 rateParam geq2jets_discrbin5 InstrMET_bin${NUMBER} 100 [0,999] >> datacard_CR_bin${NUMBER}_2016.txt
  echo R_geq2jets_discrbin6_bin${NUMBER}_2016 rateParam geq2jets_discrbin6 InstrMET_bin${NUMBER} 100 [0,999] >> datacard_SR_2016.txt
  echo R_geq2jets_discrbin6_bin${NUMBER}_2016 rateParam geq2jets_discrbin6 InstrMET_bin${NUMBER} 100 [0,999] >> datacard_CR_bin${NUMBER}_2016.txt
  echo R_geq2jets_discrbin7_bin${NUMBER}_2016 rateParam geq2jets_discrbin7 InstrMET_bin${NUMBER} 100 [0,999] >> datacard_SR_2016.txt
  echo R_geq2jets_discrbin7_bin${NUMBER}_2016 rateParam geq2jets_discrbin7 InstrMET_bin${NUMBER} 100 [0,999] >> datacard_CR_bin${NUMBER}_2016.txt
done

combineCards.py datacard_SR_2016.txt datacard_CR_bin1_2016.txt datacard_CR_bin2_2016.txt datacard_CR_bin3_2016.txt datacard_CR_bin4_2016.txt datacard_CR_bin5_2016.txt datacard_CR_bin6_2016.txt datacard_CR_bin7_2016.txt datacard_CR_bin8_2016.txt datacard_CR_bin9_2016.txt datacard_CR_bin10_2016.txt datacard_CR_bin11_2016.txt datacard_CR_bin12_2016.txt datacard_CR_bin13_2016.txt datacard_CR_bin14_2016.txt > datacard_2016.txt
