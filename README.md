# Statistical analysis for H&rarr;ZZ&rarr;2&ell;2&nu;

Statistical analysis based on templates produced by software in [this repository](https://gitlab.cern.ch/HZZ-IIHE/hzz2l2nu). It uses the [`combine`](https://cms-analysis.github.io/HiggsAnalysis-CombinedLimit) tool and the [`CombineHarvester`](http://cms-analysis.github.io/CombineHarvester/) package.


## Environment and installation

 * Recent versions of `combine` work under the SL7 operating system. Since the IIHE cluster is currently in SL6, use an SL7 container as explained [here](https://t2bwiki.iihe.ac.be/SingularityContainers). It needs to be set up at every login.
   
 * As per usual, set basic CMS environment with
   
   ```sh
   . $VO_CMS_SW_DIR/cmsset_default.sh
   ```
   This command needs to be repeated at every login.
 
 * Set up a CMSSW environment and checkout the `combine` package following the instructions [here](https://cms-analysis.github.io/HiggsAnalysis-CombinedLimit/#cc7-release-cmssw_10_2_x-recommended-version). Checkout tag `v8.0.1`. The `cmsenv` command needs to be repeated at every login.
 
 * Set up [`CombineHarvester `](https://github.com/cms-analysis/CombineHarvester). The sparse checkout described [here](https://cms-analysis.github.io/HiggsAnalysis-CombinedLimit/#combine-tool) is enough.
 
 * Compile CMSSW:
   
   ```sh
   cd $CMSSW_BASE/src
   scram b -j $(nproc)
   ```
   
 * Checkout this repository into preferred location and adjust the environment by running
   
   ```sh
   . ./env.sh
   ```
   from within its root directory. The environment needs to be setup at every login.


## Build the datacard from templates

This assumes you have already produced templates using the procedure described in the main analysis repository.

* Go to a separate directory
* Copy the `create_datacards_example.sh` script there and modify it so that it matches your configuration. In case you want to combine multiple years, you need to make one such script per year (and the datacards will have different names).
* Run the script:

```sh
sh create_datacards_XXX.sh
```

* If you want to combine different years, do it using `combineCards.py`:

```sh
combineCards.py datacard_201?.txt > datacard_combined.txt
```

* From the combined datacard, you can then proceed to making the workspace and get results: likelihood scans and impacts (see next section). Keep in mind that the "data" in the SR are already Asimov data: therefore, you need to compute "observed" limits in order to get expected ones.

The commands that we currently use can be summarized as:

```sh
text2workspace.py datacard_combined.txt -P hzz2l2nu_model:hzz2l2nu_model --no-b-only -o workspace.root
combine -d workspace.root -M MultiDimFit --algo grid --X-rtd MINIMIZER_analytic --points 201 --setParameterRanges r=0,5 --alignEdges 1 --robustFit 1 --cminDefaultMinimizerStrategy 0 --saveSpecifiedNuis=all
plot1DScan.py higgsCombineTest.MultiDimFit.mH120.root --main-label Expected
combineTool.py -d workspace.root -M Impacts -m 125 --robustFit 1 --doInitialFit --parallel 8 --X-rtd MINIMIZER_analytic --X-rtd FAST_VERTICAL_MORPH --setParameterRanges r=0,5 --cminDefaultMinimizerStrategy 0
combineTool.py -d workspace.root -M Impacts -m 125 --robustFit 1 --doFits --parallel 8 --X-rtd MINIMIZER_analytic --X-rtd FAST_VERTICAL_MORPH --setParameterRanges r=0,5 --cminDefaultMinimizerStrategy 0
combineTool.py -d workspace.root -M Impacts -m 125 -o impacts.json
mkdir fig
plotImpacts.py -i impacts.json -o fig/impacts
```


## Workspace

Create the datacard with

```sh
create_datacard.py /path/to/templates.root --channels eq0jets eq1jets --year YEAR
```

This command will print some warnings about negative bin contents in some templates. It will produce two files named (by default) `datacard.txt` and `shapes.root`. They are the datacard itself and a modified file with templates, which is referenced from the datacard. Use them to construct a RooFit workspace as follows:

```sh
text2workspace.py datacard.txt -P hzz2l2nu_model:hzz2l2nu_model --no-b-only -o workspace.root
```

The custom model used here implements a consistent scaling for the S, B, and SBI templates. The only POI r is chosen such that the signal amplitude is scaled by &radic;r. It is [important](https://hypernews.cern.ch/HyperNews/CMS/get/higgs-combination/1428/2/1/1/1.html) to skip construction of the background-only model (flag `--no-b-only`) because it would not include the B template, which is technically marked as a signal process. Without an explicit background-only model, `combine` will create it on the fly by setting r = 0 in the s+b model.


## Examples

Compute expected (asymptotic) upper limit on the signal strength with

```sh
combine -d workspace.root -M AsymptoticLimits --run blind --X-rtd MINIMIZER_analytic
```

Documentation for this method is available [here](https://cms-analysis.github.io/HiggsAnalysis-CombinedLimit/part3/commonstatsmethods/#asymptotic-frequentist-limits). With option `--run blind` real data are not used in any way. Option `--X-rtd MINIMIZER_analytic` enables [analytic minimization](https://cms-analysis.github.io/HiggsAnalysis-CombinedLimit/part2/bin-wise-stats/#analytic-minimisation) with respect to nuisance parameters controlling MC statistical uncertainties. It may make the underlying fits more stable.

A scan of the (negative logarithm of the) profiled likelihood with respect to the signal strength can be performed by running

```sh
combine -d workspace.root -M MultiDimFit --algo grid -t -1 --setParameters r=1 \
  --X-rtd MINIMIZER_analytic --points 100 --rMin 0 --rMax 5 --alignEdges 1
plot1DScan.py higgsCombineTest.MultiDimFit.mH120.root --main-label Expected
```

Further details about this method are given [here](https://cms-analysis.github.io/HiggsAnalysis-CombinedLimit/part3/commonstatsmethods/#likelihood-fits-and-scans). Options `-t -1 --setParameters r=1` instruct `combine` to generate an s+b [Asimov dataset](https://cms-analysis.github.io/HiggsAnalysis-CombinedLimit/part3/runningthetool/#asimov-datasets) instead of using real data. Typically, fits for some of the probed values of the POI will fail. Normally, this is not a problem as long as the fraction of missing points is not too large, but adding options `--cminPreScan` and/or `--cminFallbackAlgo Simplex` might help to reduce the number of failures.

Instructions to compute the impacts of individual nuisance parameters are [given](https://cms-analysis.github.io/HiggsAnalysis-CombinedLimit/part3/nonstandard/#nuisance-parameter-impacts) in `combine`'s documentation. Here is a summary:

```sh
combineTool.py -d workspace.root -M Impacts -t -1 --setParameters r=1  -m 125 --robustFit 1 \
  --X-rtd MINIMIZER_analytic --doInitialFit
combineTool.py -d workspace.root -M Impacts -t -1 --setParameters r=1  -m 125 --robustFit 1 \
  --X-rtd MINIMIZER_analytic --doFits  # Adding --parallel $(nproc) might be useful
combineTool.py -d workspace.root -M Impacts -m 125 -o impacts.json
plotImpacts.py -i impacts.json -o fig/impacts
```

As before, an s+b Asimov dataset is used instead of real data.
