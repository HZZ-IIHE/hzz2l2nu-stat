#!/usr/bin/env python

"""Constructs datacard for H->ZZ->llvv."""

import argparse

from CombineHarvester.CombineTools import ch
import uproot

import ROOT
ROOT.PyConfig.IgnoreCommandLineOptions = True


class TemplateStructure:
    """Exposes relation between channels, processes, and variations."""

    def __init__(self, path):
        self._file = uproot.open(path)

    def channels(self):
        return {d.name for d in self._file.values()}

    def processes(self, channel):
        """Processes contributing to given channel.

        Observed data are not included.
        """

        names = {d.name for d in self._file[channel].values()}
        names.discard('data_obs')
        names.discard('DYJets')
        return names

    def variations(self, channel, process):
        entries = {d.name for d in self._file[channel][process].values()}
        names = {entry[:-2] for entry in entries if entry.endswith('Up')}
        return names


if __name__ == '__main__':
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('templates', help='ROOT file with templates.')
    arg_parser.add_argument(
        '-c', '--channels', nargs='*',
        help='Channels to be included.  If not given, will use all '
        'available channels.'
    )
    arg_parser.add_argument(
        '--datacard', default='datacard.txt', help='Name for data card file.')
    arg_parser.add_argument(
        '--shapes', default='shapes.root',
        help='Name for output file with templates.')
    arg_parser.add_argument('-y', '--year', default='',
        help='Year, used to decorrelate several systematics')
    args = arg_parser.parse_args()

    templates = TemplateStructure(args.templates)

    builder = ch.CombineHarvester()
    # builder.SetVerbosity(2)  # Useful for debugging

    categories = list(enumerate(
        args.channels if args.channels else templates.channels()))
    builder.AddObservations(bin=categories)

    trueProcesses = []
    for category in categories:
        for process in templates.processes(category[1]):
            if not 'InstrMET_' in process:
                trueProcesses.append(process)
            builder.AddProcesses(
                procs=[process], bin=[category],
                signal=(process in {'GGToZZ_S', 'GGToZZ_B', 'GGToZZ_BSI'}))
            for syst in templates.variations(category[1], process):
                builder.cp().process([process]).bin([category[1]]).AddSyst(
                    builder, syst, 'shape', ch.SystMap()(1.))

    # For luminosity, use a 2.5% uncertainty regardless of the year for
    # the time being.  This is the conservative number.  In reality the
    # uncertainty depends on the year [1], and there are correlations
    # across the data taking periods.
    # [1] https://twiki.cern.ch/twiki/bin/view/CMS/TWikiLUM?rev=131#SummaryTable
    builder.cp().process(trueProcesses).AddSyst(
        builder, 'lumi_' + args.year, 'lnN', ch.SystMap()(1.025))

    builder.ExtractShapes(
        args.templates, '$BIN/$PROCESS/nominal', '$BIN/$PROCESS/$SYSTEMATIC')
    builder.SetAutoMCStats(builder, 0., True)

    output_templates = ROOT.TFile(args.shapes, 'recreate')
    builder.WriteDatacard(args.datacard, output_templates)
    output_templates.Close()

