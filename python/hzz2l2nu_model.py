from HiggsAnalysis.CombinedLimit.PhysicsModel import \
    PhysicsModelBase_NiceSubclasses

class HZZ2L2NuModel(PhysicsModelBase_NiceSubclasses):
    """Model for off-shell H->ZZ->2l2v.

    Effects of the interference are included by combining templates for
    pure H->ZZ (S), pure background (B), and full ZZ production (SBI).
    There is a single POI r.  The signal amplitude is scaled by sqrt(r).
    """

    def getPOIList(self):
        """Construct POIs and return their names.

        At the same time construct scaling functions based on the POIs.
        """

        # A single signal strength.  All pure signal amplitudes are
        # scaled by sqrt(r).
        self.modelBuilder.doVar('r[1,0,20]')

        # Scaling is done as in the example in [1]
        # [1] https://cms-analysis.github.io/HiggsAnalysis-CombinedLimit/part2/physicsmodels/#model-building
        self.modelBuilder.factory_('expr::ggH_s_func("@0-sqrt(@0)", r)')
        self.modelBuilder.factory_('expr::ggH_b_func("1-sqrt(@0)", r)')
        self.modelBuilder.factory_('expr::ggH_bsi_func("sqrt(@0)", r)')

        self.modelBuilder.factory_('expr::qqH_s_func("@0-sqrt(@0)", r)')
        self.modelBuilder.factory_('expr::qqH_b_func("1-sqrt(@0)", r)')
        self.modelBuilder.factory_('expr::qqH_bsi_func("sqrt(@0)", r)')

        return ['r'] + super(HZZ2L2NuModel, self).getPOIList()


    def getYieldScale(self, bin_, process):
        """Return scaling for given process."""

        if not self.DC.isSignal[process]:
            return 1
        scalings = {
            'GGToZZ_S': 'ggH_s_func', 'GGToZZ_B': 'ggH_b_func',
            'GGToZZ_BSI': 'ggH_bsi_func'
            # No VBF processes included at the moment
        }
        return scalings[process]


    def processPhysicsOptions(self, options):
        # Options are not used
        return super(HZZ2L2NuModel, self).processPhysicsOptions(options)


hzz2l2nu_model = HZZ2L2NuModel()

